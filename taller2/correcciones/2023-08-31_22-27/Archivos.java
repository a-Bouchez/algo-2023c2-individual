package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float [] res = new float[largo];
        for (int i = 0; i < largo; i ++){
            res[i] = entrada.nextFloat();
        }
        return res;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] matriz = new float[filas][columnas];
        for (int i = 0; i < filas; i++) {
            matriz[i] = leerVector(entrada, columnas);
        }
        return matriz;

    }

    void imprimirPiramide(PrintStream salida, int alto) {
        for (int i = 1; i <= alto; i++) {
            int espacios = alto - i;
            int asteriscos = 2 * i - 1;
            for (int a = 0; a < espacios; a++) {
                salida.print(" ");
            }
            for (int b = 0; b < asteriscos; b++) {
                salida.print("*");
            }
            for (int c = 0; c < espacios; c++) {
                salida.print(" ");
            }
            salida.println();
         
        }
    }
}
