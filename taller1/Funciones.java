package aed;

class Funciones {
    int cuadrado(int x) {
        return (x*x);
    }

    double distancia(double x, double y) {
        double res = 0;
        res = Math.sqrt( x*x + y*y);
        return res;
    }

    boolean esPar(int n) {
        if (n % 2 == 0){
           return true; 
        }
        return false;
    }

    boolean esBisiesto(int n) {
        if ((n % 4 == 0) && (n % 100 != 0)){
            return true;
        }
        if (n % 400 == 0) {
            return true;
        }
        return false;
    }

    int factorialIterativo(int n) {
        if (n == 0) {
            return 1;
        }
        int res = 1;
        while (n > 0) {
            res = res * n;
            n = n-1;
        }
        return res;
    }

    int factorialRecursivo(int n) {
         if (n == 0) {
            return 1;
        }
        return n * factorialRecursivo(n - 1);
    }

    boolean esPrimo(int n) {
        if (n == 1 || n == 0){
            return false;
        }
        for (int i = 2; (i < n); i++){
            if (n % i == 0){
                return false;
            }
        }
        return true;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for ( int i = 0; i < numeros.length; i++){
            res = res + numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int res = 0;
        for ( int i = 0; i < numeros.length; i++){
            if (numeros[i] == buscado){
                res = i;
            }
        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        for (int i = 0; i < numeros.length; i++){
            if (esPrimo(numeros[i]) == true){
                return true;
            }
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        for (int i = 0; i < numeros.length; i++){
            if (numeros[i] % 2 != 0){
                return false;
            }
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {
        if (s1.length() > s2.length()){
            return false;
        }
        for (int i = 0; i < s1.length(); i++){
            if (s1.charAt(i) != s2.charAt(i)){
                return false;
            }
        }
        return true;
    }

    boolean esSufijo(String s1, String s2) {
        return esPrefijo(new StringBuilder(s1).reverse().toString(), new StringBuilder(s2).reverse().toString());
        //new StringBuilder(hi).reverse().toString()
    }
}