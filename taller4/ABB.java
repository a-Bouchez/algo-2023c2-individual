package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo raiz;
    private int cardinal;
    private Nodo min;
    private Nodo max;

    public ABB() {
        raiz = null;
        cardinal = 0;
        min = null;
        max = null; 
    }

 private class Nodo {
        T valor;
        Nodo der;
        Nodo izq;
        Nodo padre;

        Nodo (T v) {
            valor = v;
            izq = null;
            der = null;
            padre = null;
        }
    }
    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        return min.valor;
    }

    public T maximo(){
        return max.valor;
    }

    public void insertar(T elem){
        Nodo nuevo = new Nodo(elem);
        
        if (raiz == null) {
            raiz = nuevo;
            min = nuevo;
            max = nuevo;
        } else {
            Nodo actual = raiz;
            Nodo padre = null;
        
            while (actual != null) {
                padre = actual;
             if (nuevo.valor.compareTo(actual.valor) < 0) {
                 actual = actual.izq;
                } else if (nuevo.valor.compareTo(actual.valor) > 0) {
                actual = actual.der;
                } else {
                  return;
                }
            }
            nuevo.padre = padre;
            if (nuevo.valor.compareTo(padre.valor) < 0) {
                padre.izq = nuevo;
            } else {
                padre.der = nuevo;
            }
            if (nuevo.valor.compareTo(min.valor) < 0) {
                min = nuevo;
            }
            if (nuevo.valor.compareTo(max.valor) > 0) {
                max = nuevo;
            }
        }
        cardinal = cardinal + 1;
    }

    public boolean pertenece(T elem){
        Nodo buscado = new Nodo(elem); 
        boolean res = false; 
        Nodo actual = raiz;
        while (actual != null) {
            if (actual.valor.compareTo(buscado.valor) == 0) {
            res = true;
            break;
            } else {
                 if (actual.valor.compareTo(buscado.valor) > 0) {
                        actual = actual.izq;
                    } else {
                         actual = actual.der;
                    }
            } 
        }
        return res;
    }

    public void eliminar(T elem) {
        Nodo buscado = encontrarNodo(raiz, elem);
        if (buscado != null) {
            raiz = eliminarNodo(raiz, buscado);
            cardinal--;
        }
    }
    
    private Nodo eliminarNodo(Nodo nodo, Nodo buscado) {
        if (nodo == null) {
            return nodo;
        }
    
        if (buscado.valor.compareTo(nodo.valor) < 0) {
            nodo.izq = eliminarNodo(nodo.izq, buscado);
        } else if (buscado.valor.compareTo(nodo.valor) > 0) {
            nodo.der = eliminarNodo(nodo.der, buscado);
        } else {
            if (nodo.izq == null) {
                return nodo.der;
            } else if (nodo.der == null) {
                return nodo.izq;
            }
            Nodo sucesor = encontrarSucesor(nodo.der);
            nodo.valor = sucesor.valor;
            nodo.der = eliminarNodo(nodo.der, sucesor);
        }
        return nodo;
    }
    
    private Nodo encontrarSucesor(Nodo nodo) {
        while (nodo.izq != null) {
            nodo = nodo.izq;
        }
        return nodo;
    }
    
    private Nodo encontrarNodo(Nodo nodo, T elem) {
        if (nodo == null || nodo.valor.compareTo(elem) == 0) {
            return nodo;
        }
    
        if (nodo.valor.compareTo(elem) > 0) {
            return encontrarNodo(nodo.izq, elem);
        } else {
            return encontrarNodo(nodo.der, elem);
        }
    }    

    public String toString(){
        StringBuilder res = new StringBuilder();
        Nodo actual = raiz;
        res.append("{");
        toStringInOrder(actual, res);

        if (res.length() > 1) {
            res.setLength(res.length() - 1);
        }
        res.append("}");
        return res.toString();
    }

    private void toStringInOrder(Nodo nodo, StringBuilder res) {
        if (nodo != null) {
            toStringInOrder(nodo.izq, res);
            res.append(nodo.valor);
            res.append(",");
            toStringInOrder(nodo.der, res);
        }
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual;
        private Stack<Nodo> pila;
    
        public ABB_Iterador() {
            pila = new Stack<>();
            _actual = raiz;
            while (_actual != null) {
                pila.push(_actual);
                _actual = _actual.izq;
            }
        }
    
        public boolean haySiguiente() {
            return !pila.isEmpty();
        }
    
        public T siguiente() {
            _actual = pila.pop();
            T valor = _actual.valor;
            _actual = _actual.der;
            while (_actual != null) {
                pila.push(_actual);
                _actual = _actual.izq;
            }
            return valor;
        } 
    }    

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
