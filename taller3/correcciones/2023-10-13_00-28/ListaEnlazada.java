package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private Nodo primero;
    private Nodo ultimo;

    public ListaEnlazada() {
        primero  = null;
        ultimo = null;
    }

    private class Nodo {
        T valor;
        Nodo sig;
        Nodo ant;
        
        Nodo (T v) {valor = v;}
    }

    public int longitud() {
        int res = 0;
        Nodo actual = primero;

        while (actual != null) {
            res += 1;
            actual = actual.sig;
        }
        return res;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo (elem);
        nuevo.sig = primero;
        primero = nuevo;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo =  new Nodo (elem);
        if (primero == null) {
            primero = nuevo;
        } else {
            Nodo actual = primero;
            while (actual.sig != null) {
                actual = actual.sig;
            }
            actual.sig = nuevo;
        }
    }

    public T obtener(int i) {
        Nodo actual = primero;
        int j = 0;

        while (j != i){
            actual = actual.sig;
            j ++;
        }
        return actual.valor;
    }

    public void eliminar(int i) {
        Nodo actual = primero;
        Nodo prev = primero;
        for (int j = 0; j < i; j++) {
            prev = actual;
            actual = actual.sig;
        }
        if (i == 0) {
            primero = actual.sig;
        } else {
            prev.sig = actual.sig;
        }
    }

    public void modificarPosicion(int indice, T elem) {
       Nodo actual = primero;
       int j = 0;

       while (j != indice) {
        j++;
        actual = actual.sig;
       }

       actual.valor = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> nuevo = new ListaEnlazada<>();
        Nodo actual = primero;

        while (actual != null) {
           T elem = actual.valor;
           nuevo.agregarAtras(elem);
           actual = actual.sig;
        }
        
        return nuevo;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        Nodo actual = lista.primero;

        while (actual != null) {
            agregarAtras(actual.valor);
            actual = actual.sig;
        }
    }
    
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        Nodo actual = primero;

        res.append("[");
        
        while (actual != null) {
            res.append(actual.valor);
            if (actual.sig != null) {
                res.append(", ");
            }
            actual = actual.sig;
        }

        res.append("]");

        return res.toString();
    }

    private class ListaIterador implements Iterador<T> {
    	private int dedito;
        private ListaEnlazada<T> lista;  

        public ListaIterador (ListaEnlazada<T> lista) {
            this.lista = lista;
            dedito = 0;
        }

        public boolean haySiguiente() {
	      return dedito < lista.longitud();
        }
        
        public boolean hayAnterior() {
	       return dedito > 0;
        }

        public T siguiente() {
	        int i = dedito;
            dedito = dedito + 1;
            return lista.obtener(i);
        }
        

        public T anterior() {
	        int i = dedito;
            dedito = dedito - 1;
            return lista.obtener(i - 1);
        }
    }

    public Iterador<T> iterador() {
	    return new ListaIterador(this);
    }

}
