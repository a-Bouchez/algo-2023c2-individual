package aed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        insertionSort(fragments);
        return fragments;
    // Pasa con insertion que es O(n^2) en el promedio pero no con countSort buscando el max o con Radix que cumplen las complejidades teoricas
    }

    public static void insertionSort(Fragment[] array) {
        int n = array.length;
        
        for (int j = 1; j < n; j++) {
            Fragment a = array[j];
            int i = j - 1;
            
            while ((i > -1) && (array[i].compareTo(a) > 0)) {
                array[i + 1] = array[i];
                i--;
            }
            
            array[i + 1] = a;
        }
    }
    
    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        List<Router> routersFiltrados = new ArrayList<>();
        for (Router router : routers) {
            if (router.getTrafico() > umbral) {
                routersFiltrados.add(router);
            }
        }
        Router[] result = routersFiltrados.toArray(new Router[0]);
        heapSort(result, result.length);

        int left = 0, right = result.length - 1;
        while (left < right) {
            Router temp = result[left];
            result[left] = result[right];
            result[right] = temp;
            left++;
            right--;
        }

        if (k < result.length) {
            return Arrays.copyOfRange(result, 0, k);
        } else {
            return result;
        }
    }
    

    private void heapify(Router[] routers, int n, int i) {
        int raiz = i;
        int izq = 2 * i + 1;
        int der = 2 * i + 2;

        if (izq < n && routers[izq].compareTo(routers[raiz]) > 0)
            raiz = izq;

        if (der < n && routers[der].compareTo(routers[raiz]) > 0)
            raiz = der;

        if (raiz != i) {
            Router temp = routers[i];
            routers[i] = routers[raiz];
            routers[raiz] = temp;

            heapify(routers, n, raiz);
        }
    }

    private void heapSort(Router[] routers, int n) {
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(routers, n, i);

        for (int i = n - 1; i >= 0; i--) {
            Router temp = routers[0];
            routers[0] = routers[i];
            routers[i] = temp;

            heapify(routers, i, 0);
        }
    }
  

    public IPv4Address[] sortIPv4(String[] ipv4) {
        int n = ipv4.length;
        IPv4Address[] addresses = new IPv4Address[n];
    
        for (int i = 0; i < n; i++) {
            addresses[i] = new IPv4Address(ipv4[i]);
        }
        // insertionSort adecuado a IPv4Address
        for (int j = 1; j < n; j++) {
            IPv4Address current = addresses[j];
            int i = j - 1;
    
            while (i > -1 && compareIPv4(addresses[i], current) > 0) {
                addresses[i + 1] = addresses[i];
                i--;
            }
    
            addresses[i + 1] = current;
        }
    
        return addresses;
    }
    
        private int compareIPv4(IPv4Address a, IPv4Address b) {
            for (int i = 0; i < 4; i++) {
                int diff = a.getOctet(i) - b.getOctet(i);
                if (diff != 0) {
                    return diff;
                }
            }
            return 0; 
        }


}
